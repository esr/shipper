= shipper project news =

1.21: 2024-02-17::
  Accept Golang-style senantic-version numbers with a v prefix.
  Do early check for version matching an existing tag.
  Whwn a NEWS version is available, check what's passed in. 

1.20: 2024-02-08::
  Ignore // comments in .adoc files.

1.19: 2020-08-03::
  Sync with gitlab-igor 0.1.0

1.18: 2020-07-31::
  Second Igor integration test.

1.17: 2020-07-30::
  Test release for Igor integration.

1.16: 2019-04-26::
  Update the default template to emit HTML5 with a stylesheet.

1.15: 2019-03-20::
  Shipper can now do broadcast notifications on Twitter.
  Added Debian package name as a metadata field.
  Add size attributes for logo inclusions.

1.14: 2019-03-19::
  Do the right thing if README etc. are actually *.adoc files.

1.13: 2018-11-27::
  Add capability to set the background image of the templated web page.
  Add sanity checking of NEWS release dates

1.12: 2016-01-29::
  Forward-port to Python 3.

1.11: 2016-01-12::
  Fix a bug that caused shipper -w of a SourceForge project to screw up.

1.10: 2015-09-02:
  Stymie a buffering bug in Python that caused short writes piping to a shell.
  Recognize and ship xz archives.

1.9: 2015-08-23::
  Switch to Patreon from Gratipay, which no longer supports individuals.

1.8: 2015-05-24::
  Per-project shipper variables can now be set from git config section.
  Removal of Berlios support, since I've learned that it shut down in 2012.
  The -w option now updates both web pages and gitweb metadata.

1.7: 2015-04-05::
  Adapted for Gitorious shutdown; Gitorious-URL and Github-URL
  replaced by Repository-URL, Developer-Clone, and Anonymous-Clone.
  Now makes a gitweb-style description and README.html file.

1.6: 2014-01-07::
  Fix a brown-paper-bag bug in SourceForge directory syncing.

1.5: 2014-10-29::
  Add the ability to ship notifications to freshcode. Gittip has changed
  its name to Gratipay.  Ohloh has changed its name to OpenHub.

1.4: 2014-06-24::
  More permissive NEWS parsing. Interpret leading version on a news stanza.
  Recent changes is now part of the default web-page template.

1.3: 2014-06-19::
  Removing freecode.com support, as it stopped accepting new updates yesterday.

1.2: 2014-06-05::
  Fix a bug in the SourceForge delivery code.

1.1: 2014-06-04::
  Added the Validate field for smoke-testing before deliverables are shipped.

1.0: 2014-06-01::
  First public production release.

0.23: 2014-05-20::
  Correct a bug in Freecode name processing.

0.22: 2013-12-28::
  Minor bug fixes in web page templating.

0.21: 2013-12-02::
  Manual page overhauled in reponse to user feedback.

0.20: 2013-12-02::
  Project-Tag-List -> Freecode-Tags
  There is now the beginnings of a regression-test suite.

0.19: 2013-11-30::
  Use irkerd's new (release 2.3) immediate mode for IRC notifications.

0.18: 2013-11-22::
  Major change in interface; this is now a shellscript generator
  New variable-override syntax on the command line.
  Your profile can now be ~/.config/shipper a la XDG.

0.17: 2013-11-16::
  Added support for announcing to project IRC channels via irk.
  Helpers required by the package are now documented.

0.16: 2013-09-24::
  Added support for embedding a gittip link.

0.15: 2013-05-23::
  The Fernando Poo Day double release! First spin of 0.15 had a wrong checksum.
  SourceForge upload locations for FRS and project web have changed.
  Add and document the the sourceforge-folder variable.
  Ship project description with updates.
  Added IRC chat channel and Ohloh/github/gitorious URLs to optional metadata.
  Extract HTML page descriptions from <title> elements.
  Push changes and tags if -t and -u are both enabled.
  md5 and sha* checksum files added to automatic website uploads.
  Release-Focus is gone; calling shipper is now a fire-and-forget operation.
  Add untested support for savannah-nongnu.

0.14: 2011-11-03::
  Cope with the Freshmeat to Freecode name change.

0.13: 2010-12-01::
  Set the Berlios download location correctly for SourceForge announcements.

0.12: 2010-11-26::
  Can now tag releases under hg and bzr,
  Added the logo variable and logo embedding in the web-page template.

0.11: 2010-11-22::
  Support for SourceForge as a destination.

0.10: 2010-11-15::
  Generate correct download directories in a Freshmeat announcement for a 
  Berlios project.  Check version in makefile as well as Makefile.

0.9: 2005-04-03::
  The Channels variable is gone. There are no longer default public channels;
  you put the ones you want in your Destinations variable.  For safety's sake
  the force (-f) option is also gone; generated deliverables are now built 
  unconditionally, and you must explicitly make sure no index.html exists 
  in order to get one generated.  There is now a "berlios" channel.

0.8: 2005-02-01::
  Strip trailing edit-mode lines out of RPM spec files.

0.7: 2005-01-27::
  Now handles packages that generate multiple binary RPMs correctly.

0.6: 2004-08-21::
  Fixed a minor bug in the generation of lftp commands.  Also, generate
  a To line into email announcement.  Mailman doesn't like implicit
  addressing. 

0.5: 2004-02-06::
  Added security check so the ~/.shipper and .shipper files can't be used
  for privilege elevation.  Fixed upload omission bug in case where neither 
  -n nor -f was on and the webpage wasn't being built.  Deliverables 
  created for upload are deleted at end of run.

0.4: 2004-01-11::
  Correct extraction of freshmeat name.  Build generated deliverables
  only if we know they will be needed. Help is now available at the 
  freshmeat-focus prompt.

0.3: 2004-01-10::
  First alpha release of unified shipper package.  It can ship itself.

0.2: 2003-12-17::
  rpm2lsm now grabs an RPM from the current directory if no argument,
  and parses an AUTHORS file if present (GNU convention).  Also,
  this release fixes a bug in USERNAME handling.

0.1: 2002-08-01::
  Initial release of rpm2lsm, since folded into shipper package.
